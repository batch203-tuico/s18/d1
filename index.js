// console.log("Hello World");

// We learned how to gather data from the user input using a prompt();
function printInfo(){
	let nickname = prompt("Enter your nickname");
	console.log("Hi, " + nickname);
}

// printInfo();


function printName(name){
	console.log("My name is " + name);
}

printName("Juana");
printName("John");

// [SECTION] Parameters and Arguments

// Parameter
	// "name" is called a parameter
	// A "parameter" acts a named variable/container that exists only inside the function
	// It is used to store information that is provided to a function when it is called/invoke.

// Argument
	// "Juana" and "John", the information/data provided directly into the function is called "argument".
	// Values passed when invoking a function are called arguments.
	// These arguments are then stored as the parameter within the function.

// Variables can also be passed as an argument
	let sampleVariable = "Yui"

	printName(sampleVariable);
	printName(); // function calling without arguments will result to undefined parameters.

	// Check if a specific number is divisible by 8

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}
	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);

// [SECTION] Function as argument
	// Function parameters can also accept other functions as arguments.
	// Some complex functions uses other functions as arguments to perform more complicated result.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	// A function used without a parenthesis is normally associated with using the function as an argument to another function.
	invokeFunction(argumentFunction);

	console.log(argumentFunction);

// [SECTION] Using Multiple Parameters
	// Multiple "arguments" will corresponds to the number of "parameters" ddeclared in a function in succeeding order.

	function createFullName(firstName, middleName, lastName){
		console.log("My full name is " + firstName + " " + middleName + " " + lastName);
	}
					//fName       //mName    //lName
	createFullName("Geward John", "Sayson", "Tuico");

	// In Javascript providing more/less arguments than expected parameters will not return an error.
	// In other programming language, this will return an error stating that "the expected number of argument do not match the number of parameters".
	createFullName("Geward John", "Sayson"); //resulted to undefined last argument value

	createFullName("Geward John", "Sayson", "Tuico", "Gee"); // no error - code will still work but excess argument will not be displayed.


	// Using varibles as arguments
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);


// [SECTION] The return statement
	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called.

	function returnFullName(firstName, middleName, lastName){

		// You can also create a variable inside a function to containt the result and return the variable.
		// let fullName = firstName + " " + middleName + " " + lastName;
		// return fullName;
		return firstName + " " + middleName + " " + lastName;
		// This line of code will not be printed.
		// Line of code after the return statement will not be read.
		console.log("This is printed inside the function");

	}

	// Value returned from the function is stored in our variable.
	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName);

	// Using return statement value, we can further use/manipulate in our program instead of only printing/displaying in our console log.
	console.log("My name is " + completeName);

	console.log(returnFullName("Jeffrey", "Smith", "Bezos"));

	// when a function only have console.log() it result to undefined.
	function printPlayerInfo(userName, level, job){
		console.log("Username: " + userName);
		console.log	("Level: " + level);
		console.log("Job: " + job);
	}

	let user1 = printPlayerInfo("knight_white", 95, "Paladin");
	console.log(user1); //returns undefined